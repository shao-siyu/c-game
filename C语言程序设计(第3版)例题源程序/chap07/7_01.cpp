/*【例7-1】输入10个整数，计算这些数的平均值，再输出所有大于平均值的数。*/

/* 输出所有大于平均值的数 */
#include<stdio.h>
int main(void)
{
   int i; 
   double average, sum;       /* average存放平均值，sum保存数据之和 */
   int a[10];    	      /* 定义1个数组a，它有10个整型元素*/

   printf("Enter 10 integers: ");	/* 提示输入10个数 */
   /* 将输入数依次赋给数组a的10个元素a[0]～a[9]（如图7.1所示），并求和*/
   sum = 0;
   for(i =0; i<10; i++){
      scanf ("%d", &a[i]); 
      sum = sum + a[i]; 
   }
   average = sum / 10;	     /* 求平均值*/
   printf("average = %.2f\n", average);
   printf(">average:");
   for(i = 0; i<10; i++){    /* 逐个与平均值比较，输出大于平均值的数*/
      if(a[i]> average)
         printf("%d ", a[i]);
   }
   printf("\n");

   return 0;
}

